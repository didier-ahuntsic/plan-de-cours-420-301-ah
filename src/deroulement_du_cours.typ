#let section-counter = counter("section-counter")

#let section(content) = {
    section-counter.step()
    let idx = section-counter.display();
    heading(level: 3)[ Partie #idx: #content ];
}

#let stupid-table-counter = counter("stupid-table-counter")

#let stupid-table-item() = {
    stupid-table-counter.step();
    stupid-table-counter.display();
}


#let mise-en-place = [
Mise en place de l'environnement de développement

- Installation
- Configuration
- Tests
]

#let notion-es5 = [
Notion de classe en ES5
- Prototype
- Gestion de plusieurs objets
- Tableaux d'objets
- Copie d'objets
- Passage d'objets en paramètre
- Comparaison d'objets
]

#let classes-and-es6 = [
Création de classe en ES6

- Définition d'une classe, attribut et méthode
- Visibilité d'une classe et de ses membres, paquetages
- Instanciation d'un objet, constructeur
- Surcharge de méthodes
- Distinction entre membre de classe et d'instance.

Documentation d'un programme objet

- Documentation de la conception
    - Introduction à UML (Unified Modeling Langage)
    - Le diagramme de classe

]

#let pure-oop-bullshit = [
- Composition d'objets
- Représentation graphique de la composition en UML (Relation « a un »)
- Héritage
- Représentation graphique de l'héritage en UML (Relation « est un »)
- Protection des données
- Polymorphisme
- Redéfinition des méthodes
]

#let imaginary-topics = [
- Classes abstraites
- Interfaces
- Conteneurs
]

#let boiler-plate = [
==== Méthodologie

Une présentation des concepts théoriques et des démonstrations pratiques par l'enseignant permettra à l'élève de réaliser les activités d'apprentissage.

==== Activités d'apprentissage
Exercices à faire en classe et à compléter à la maison.


==== Activités d'apprentissage
Exercices et laboratoire.
]


#let deroulement-du-cours = [

#section([Preparer l’environnement et eléments avances en es5])

==== Objectifs spécifiques
À l'issue de cette étape, l’étudiant capable de préparer l’environnement de développement informatique et apprendre des aspects avancés de ES5.

==== Contenu

Préparation de l’environnement de développement informatique et aspects avancés de ES5
- Installation d’un environnement de développement pour utiliser avec ES5 et ES6
- Manipulation du DOM (Document Object Model)
- Notion de classe en ES5
- JavaScript et Web

#boiler-plate

#section([La programmation orientée objet en ES6])

==== Objectifs spécifiques
À l'issue de cette étape, l'élève sera capable de concevoir des programmes utilisant les aspects de base de la programmation orientée objet en ES6


==== Contenu
- Notions de classe, instanciation et objets
- Héritage et composition
- Modélisation de classes
- Surcharge de méthodes
- Distinction entre membre de classe et d’instance
- Documentation d’un programme objet

#boiler-plate

#section([Aspects avancés de la programmation orientée objet en ES6])

=== Objectifs spécifiques

À l'issue de cette étape, l'élève sera capable de concevoir des programmes utilisant les aspects de avancés de la programmation orientée objet en ES6

=== Contenu

- Protection des données
- Polymorphisme
- Redéfinition des méthodes
- Classes abstraites
- Interfaces
- Exceptions

#boiler-plate


=== Programmation

#table(
  columns: (auto, 1fr, auto),
  inset: 10pt,
  align: horizon,
  [], [], [*Priorité \ (semaine)*],
  stupid-table-item(), mise-en-place, [1],
  stupid-table-item(), [Manipulation du DOM (Document Object Model)], [1],
  stupid-table-item(), notion-es5, [3],
  stupid-table-item(), classes-and-es6, [3],
  stupid-table-item(), pure-oop-bullshit, [4],
  stupid-table-item(), imaginary-topics, [2],
  stupid-table-item(), [Gestion des Exceptions], [1],
)

]
