#let _ponderation = link("https://www.cegepsquebec.ca/nos-cegeps/presentation/systeme-scolaire-quebecois/grille-de-cours-et-ponderation/")[Pondération]

#let frontpage = [ 
#align(center)[
    #image("logo-ahuntsic.png", width: 33%),

    #text(size: 20pt)[Plan De Cours]

    #text(size: 12pt)[Automn 2023]
]

#table(
  columns: (auto, 1fr),
  inset: 25pt,
  align: horizon,
  [Titre du cours],[*Programmation Web côté client II*],
  [Code],[*420-301-AH*],
  _ponderation, text(weight: "bold", font: "FreeMono")[1-3-2],
  [Compétences visées], list(
        [*AF56 - Utiliser un langage de programmation (atteinte partielle).*],
        [*AF57 - Effectuer le développement d’applications Web transactionnelles (atteinte partielle).*],
        [*00Q6 - Exploiter les principes de la programmation orientée objet (atteinte partielle). *],
    ),
  [Unités], [2],
  [Préalable], [
    - 420-301-AH - Programmation Web côté client I
  ],
  // [Corequis], [Aucun],
  [Enseignant], [
    - *Didier Amyot*
    - #link("mailto:didier.amyot@collegeahuntsic.qc.ca")[didier.amyot\@collegeahuntsic.qc.ca]
    ],
  [Département], [*Informatique*]
)
]

