#let course-bibliography = [
// Bellow is a little hack to display bibliography 
// whitout using them in the text.  Indeed, we use
// them in a text but we set the size to 0.
#text(size: 0em)[
    @javascript_the_good_part
    @exploring_es6
    @javascript_oop
    @mozilla
    @deno_manual
    @nodejs_doc
    @typescript
]



#bibliography(title: "Bibliography", "bibliography.yaml")


]
