#let generalites = [
== Présentation générale

Ce cours du 2#super[ime] bloc poursuit l'apprentissage de développement d'applications Web entrepris dans le premier cours de programmation Web 
côté client. Il aborde plus particulièrement l'apprentissage des techniques de programmation orientée objet (POO) à l'aide du langage de
scripts JavaScript. Il présente la programmation orientée objet dans le contexte de ECMAScript 5 (ES5) et ECMAScript 6 (ES6). 
À l'issue de ce cours, l'étudiant sera en mesure de développer une application Web côté client utilisant les notions de POO.
Les objectifs intermédiaires de ce cours sont d'utiliser les concepts de la programmation orientée objet, de créer, valider et programmer un
modèle objet, programmer une interface utilisateur pour une application répondant aux besoins de l'utilisateur, d'exploiter des données en
mémoire en utilisant les conteneurs d'objet et de valider le fonctionnement d'une application.


Les principaux thèmes abordés dans de ce cours sont: les éléments de la POO dans le contexte du Web; la création de conteneurs d'objets 
et la disposition du contenu de ces objets dans la page du client.

== Principales activités d’apprentissage

En classe (théorie et laboratoire), l’étudiant explore les divers concepts présentés par le professeur afin d’élaborer, dans l’environnement de développement, une application Web côté client utilisant la POO. Comme travail personnel, l’étudiant complète ses laboratoires et révise la théorie.

]
