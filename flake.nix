{
  description = "The Syllabus of 420-301-AH";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    adp-nix-utils.url = "gitlab:all-dressed-programming/adp-nix-utils";
  };

  outputs = { self, nixpkgs, flake-utils, adp-nix-utils }:
     flake-utils.lib.eachDefaultSystem (system:
       let
         pkgs = nixpkgs.legacyPackages.${system};
         make-mprocs = adp-nix-utils.make-mprocs.${system};
         git-hash = if (self ? rev) then builtins.substring 0 8 self.rev else "dirty";
         last-updated = if (self ? lastModified) then builtins.toString self.lastModified else "0";
         syllabus-fonts = pkgs.stdenv.mkDerivation {
          name = "syllabus-fonts";
          buildInputs = [pkgs.font-awesome pkgs.fira-code pkgs.bakoma_ttf];
          src =  builtins.filterSource (path: type: false) ./.;
          buildPhase =  ''
            mkdir build
            cp -r ${pkgs.font-awesome}/share/fonts/opentype/* build
            cp -r ${pkgs.fira-code}/share/fonts/truetype/* build
            cp -r ${pkgs.bakoma_ttf}/share/fonts/truetype/* build
            '';
          installPhase =  ''mv build $out'';
         };
         watch-dev = pkgs.writeShellApplication {
            name = "watch-dev";
            runtimeInputs = [pkgs.typst];
            text = ''
                cd "$(dirname "$DIRENV_FILE")"
                exec ${pkgs.typst}/bin/typst watch --root src src/main.typ
            '';
         };
         syllabus = pkgs.stdenv.mkDerivation {
            name = "syllabus";
            src = ./.;
            buildInputs = with pkgs; [typst syllabus-fonts yq git];
            buildPhase = ''
                export TYPST_FONT_PATHS=${syllabus-fonts}
                export LAST_UPDATED_STRING=`date -u +"%Y-%m-%dT%H:%M:%SZ"  -d @${last-updated}`
                tomlq --toml-output  ".language = \"fr\" | .githash = \"${git-hash}\" | .lastUpdated = \"$LAST_UPDATED_STRING\"" src/main.toml > src/main-tmp.toml
                cp src/main-tmp.toml src/main.toml
                ${pkgs.typst}/bin/typst compile src/main.typ syllabus.pdf
                '';
            installPhase =  ''
                mkdir $out
                cp src/main.toml $out
                mv syllabus.pdf $out/plan-de-cours-420-301-ah.pdf
                '';
         };
        # syllabus-en = syllabus "en";
       in
       {
         packages = {
          syllabus-fonts = syllabus-fonts;
          default = syllabus;
          watch-dev = watch-dev;
         };
         devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [typst
                                      syllabus-fonts
                                      watch-dev
                                      yq];
            shellHook = '' 
            export TYPST_FONT_PATHS=${syllabus-fonts}
            '';
         };
       }
     );
}
